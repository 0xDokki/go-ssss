package ssss

import (
	"fmt"
	"math/big"
)

func Deconstruct(segments ...Segment) ([]byte, error) {
	res := make([]byte, 0)

	for i, seg := range segments {
		segRes := new(big.Rat)
		for j, p := range seg.Points {
			l := lagrange(j, 0, seg.Points)
			y := new(big.Rat).SetInt(p.Y)
			segRes.Add(segRes, l.Mul(l, y))
		}

		if !segRes.IsInt() {
			return nil, fmt.Errorf("invalid result fraction in segment %d: %s", i, segRes.String())
		}

		S := segRes.Num()
		S.Mod(S, seg.FieldSize)

		res = append(res, S.Bytes()...)
	}

	return res, nil
}

func lagrange(j, x int, points []Point) *big.Rat {
	res := big.NewRat(1, 1)
	for m := range points {
		if m == j {
			continue
		}
		num := big.NewInt(int64(x - points[m].X))
		denom := big.NewInt(int64(points[j].X - points[m].X))
		elem := new(big.Rat).SetFrac(num, denom)
		res.Mul(res, elem)
	}

	return res
}
