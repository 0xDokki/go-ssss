package main

import (
	"fmt"
	"gitlab.com/0xDiddi/go-ssss"
	"math/big"
)

func main() {
	secret := "Hello world, this is a test."

	// this is M_82589933, the largest known (mersenne) prime at 24 million digits
	x := big.NewInt(0).Exp(big.NewInt(2), big.NewInt(82589933), nil)
	x.Sub(x, big.NewInt(1))

	segment := ssss.ConstructSingleFixed([]byte(secret), x, 20, 15)

	result1, _ := ssss.Deconstruct(segment)
	fmt.Printf("Successful deconstruct: %s\n", string(result1))

	segWithJustEnoughPoints := ssss.Segment{
		Points:    segment.Points[:15],
		FieldSize: segment.FieldSize,
	}
	result2, _ := ssss.Deconstruct(segWithJustEnoughPoints)
	fmt.Printf("Successful deconstruct with just enough points: %s\n", string(result2))

	segWithTooFewPoints := ssss.Segment{
		Points:    segment.Points[:14],
		FieldSize: segment.FieldSize,
	}
	result3, _ := ssss.Deconstruct(segWithTooFewPoints)
	// we limit here to 256 bytes to avoid 24mb of junk in the console
	fmt.Printf("Successful deconstruct with too few points: %s\n", string(result3[:256]))

}
