package ssss

import (
	"crypto/rand"
	"math/big"
)

type Point struct {
	X int
	Y *big.Int
}

type Segment struct {
	Points    []Point
	FieldSize *big.Int
}

func GenerateCoefficients(fieldSize *big.Int, degree int) (coefficients []*big.Int) {
	coefficients = make([]*big.Int, degree)
	// we don't assign the first coefficient here since that's where the secret goes
	for i := 1; i < degree; i++ {
		coefficients[i], _ = rand.Int(rand.Reader, fieldSize)
	}
	return
}

func GeneratePoints(coefficients []*big.Int, fieldSize *big.Int, minX, maxX int) (P []Point) {
	// make sure our bounds are in the right order
	if minX > maxX {
		minX, maxX = maxX, minX
	}

	P = make([]Point, maxX-minX)
	for i := range P {
		y := polynomial(coefficients, big.NewInt(int64(i+minX)))
		y.Mod(y, fieldSize)

		P[i] = Point{
			X: i + 1,
			Y: y,
		}
	}
	return
}

func polynomial(coefficients []*big.Int, x *big.Int) *big.Int {
	y := new(big.Int)
	xx := new(big.Int)
	for i, n := range coefficients {
		xx.Exp(x, big.NewInt(int64(i)), nil).Mul(xx, n)
		y.Add(y, xx)
	}
	return y
}
