package ssss

import (
	"crypto/rand"
	"math/big"
)

func ConstructSingleFixed(secret []byte, fieldSize *big.Int, N, k int) Segment {
	S := new(big.Int).SetBytes(secret)

	coefficients := GenerateCoefficients(fieldSize, k)
	coefficients[0] = S

	return Segment{
		Points:    GeneratePoints(coefficients, fieldSize, 1, 1+N),
		FieldSize: fieldSize,
	}
}

func ConstructChunkedFixed(secret []byte, fieldSize *big.Int, N, k, chunkSize int) []Segment {
	segs := make([]Segment, 0)

	for i := 0; i < len(secret); i += chunkSize {
		bound := i + chunkSize
		if bound > len(secret) {
			bound = len(secret)
		}
		segs = append(segs, ConstructSingleFixed(secret[i:bound], fieldSize, N, k))
	}

	return segs
}

func ConstructChunkedRandom(secret []byte, N, k, chunkSize int) ([]Segment, error) {
	fieldSize, err := rand.Prime(rand.Reader, chunkSize*8)
	if err != nil {
		return nil, err
	}

	return ConstructChunkedFixed(secret, fieldSize, N, k, chunkSize), nil
}
